﻿CREATE TABLE public.estudiante
(
  id bigint NOT NULL primary key,
  nombres character varying(100) NOT NULL,
  apellido_paterno character varying(100) NOT NULL,
  apellido_materno character varying(100),
  fecha_nacimiento date NOT NULL,
  telefono_celular character varying(20)
); 

CREATE SEQUENCE public.estudiante_id_sequence
  INCREMENT 1
  MINVALUE 1
  START 1
  CACHE 1;