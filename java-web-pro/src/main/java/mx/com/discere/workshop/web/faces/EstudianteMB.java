package mx.com.discere.workshop.web.faces;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.com.discere.workshop.domain.Estudiante;
import mx.com.discere.workshop.services.EstudianteService;
import mx.com.discere.workshop.web.util.FacesUtil;

@ManagedBean
@ViewScoped
public class EstudianteMB implements Serializable {

    private static final long serialVersionUID = 2763105131057153235L;
    private static final Logger logger = LogManager.getLogger(EstudianteMB.class);

    @EJB
    private EstudianteService estudianteService;
    private List<Estudiante> estudiantes;
    private Estudiante estudiante;

    @PostConstruct
    public void init() {
        estudiante = new Estudiante();
        obtenerEstudiantes();
    }

    private void obtenerEstudiantes() {
        try {
            estudiantes = estudianteService.obtenerTodos();
        } catch (Exception e) {
            logger.error("Error al obtener los estudiantes.", e);
            FacesUtil.addErrorMessage("Error al obtener la lista de estudiantes.");
        }
    }

    public void guardar() {
        try {
            estudianteService.guardar(estudiante);
            FacesUtil.addMessage("Datos guardados correctamente");
            estudiante = new Estudiante();
        } catch (Exception e) {
            logger.error("Error al guardar los datos del estudiante.", e);
            FacesUtil.addErrorMessage("Error al guardar los datos del estudiante.");
        }
    }

    public List<Estudiante> getEstudiantes() {
        return estudiantes;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

}
