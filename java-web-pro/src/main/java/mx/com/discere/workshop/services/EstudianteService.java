package mx.com.discere.workshop.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import mx.com.discere.workshop.domain.Estudiante;

@Stateless
public class EstudianteService {
    @PersistenceContext(unitName = "taller-pu")
    private EntityManager entityManager;

    public void guardar(Estudiante estudiante) {
        entityManager.persist(estudiante);
    }

    public List<Estudiante> obtenerTodos() {
        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Estudiante> cQuery = builder.createQuery(Estudiante.class);
        final Root<Estudiante> root = cQuery.from(Estudiante.class);
        cQuery.select(root);
        cQuery.orderBy(builder.asc(root.get("id")));
        return entityManager.createQuery(cQuery).getResultList();
    }
}
