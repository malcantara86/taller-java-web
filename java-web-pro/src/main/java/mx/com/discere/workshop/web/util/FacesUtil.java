package mx.com.discere.workshop.web.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public final class FacesUtil {

	public static void addMessage(String msg) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(msg));
	}

	public static void addErrorMessage(String msg) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null));

	}

	public static void addErrorMessage(String msg, String target) {
		FacesContext.getCurrentInstance().addMessage(target, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null));

	}
}
