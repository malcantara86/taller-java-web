README

1. Para configurar el datasource para el proyecto java-web-servlet-jsp,seguir los siguientes pasos:
  - Copiar la libreria "postgresql-9.4-1201-jdbc41.jar" a "C:\apache-tomcat-8.0.35\lib"
  - Verificar que se haya creado el servidor Apache Tomcat 8
  - En la vista "Project explorer" ir a "Servers" > "Tomcat v8.0 Server" at localhost-config
  - Abrir el archivo "context.xml"
  - Agregar la definicion del data source dentro las etiquetas "<context></context>":
    <Resource name="jdbc/postgres" auth="Container" type="javax.sql.DataSource"
        driverClassName="org.postgresql.Driver" url="jdbc:postgresql://127.0.0.1:5432/java_web"
        username="postgres" password="1" maxTotal="20" maxIdle="10"
        maxWaitMillis="-1" />
  - Guardar cambios.
  
2. Para configurar el datasource para el proyecto java-web-pro, seguir los siguientes pasos:
  - Copiar la libreria "postgresql-9.4-1201-jdbc41.jar" a "C:\apache-tomee-plume-7.0.0-M3\lib"
  - Verificar que se haya creado el servidor Apache TomEE 7
  - En la vista "Project explorer" ir a "Servers" > "TomEE 7 Full Server" at localhost-config
  - Abrir el archivo "tomee.xml"
  - Agregar la definicion del data source dentro las etiquetas "<tomee></tomee>":
    <Resource id="TallerPostgreSQLDS" type="DataSource">
          jdbcDriver=org.postgresql.Driver
          jdbcUrl=jdbc:postgresql://127.0.0.1:5432/java_web
          userName=postgres 
          password=1 
          JtaManaged=true
	</Resource>
  - Guardar cambios