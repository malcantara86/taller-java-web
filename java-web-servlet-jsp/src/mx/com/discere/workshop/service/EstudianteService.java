package mx.com.discere.workshop.service;

import java.sql.SQLException;
import java.util.List;

import mx.com.discere.workshop.dao.EstudianteDAO;
import mx.com.discere.workshop.values.Estudiante;

public class EstudianteService {

    private EstudianteDAO estudianteDAO;

    public EstudianteService() {
        estudianteDAO = new EstudianteDAO();
    }

    public List<Estudiante> obtenerEstudiantes() throws Exception {
        try {
            return estudianteDAO.obtenerEstudiantes();
        } catch (SQLException e) {
            throw new Exception("Error SQL al obtener la lista de estudiantes.", e);
        }
    }

    public void guardar(Estudiante estudiante) throws Exception {
        try {
            estudianteDAO.guardar(estudiante);
        } catch (SQLException e) {
            throw new Exception("Error SQL al guardar los datos del estudiante.", e);
        }
    }
}
