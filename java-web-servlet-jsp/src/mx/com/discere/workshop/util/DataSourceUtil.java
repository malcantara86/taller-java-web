package mx.com.discere.workshop.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public final class DataSourceUtil {
    private DataSourceUtil() {

    }

    public static Connection getConnection() throws Exception {
        try {
            final InitialContext context = new InitialContext();
            return ((DataSource) context.lookup("java:/comp/env/jdbc/postgres")).getConnection();
        } catch (NamingException | SQLException e) {
            throw new Exception("Could not get connection.", e);
        }
    }
}
