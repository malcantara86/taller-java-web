package mx.com.discere.workshop.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.discere.workshop.service.EstudianteService;
import mx.com.discere.workshop.values.Estudiante;

@WebServlet(name = "AltaEstudianteServlet", urlPatterns = { "/AltaEstudiante.do" })
public class AltaEstudianteServlet extends HttpServlet {

    private static final long serialVersionUID = 280765373010385591L;
    private EstudianteService estudianteService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            final Estudiante estudiante = new Estudiante();
            estudiante.setNombres(req.getParameter("nombres"));
            estudiante.setApellidoPaterno(req.getParameter("apellidoPaterno"));
            estudiante.setApellidoMaterno(req.getParameter("apellidoMaterno"));
            estudiante.setTelefonoCelular(req.getParameter("telefonoCelular"));
            estudiante
                    .setFechaNacimiento(new SimpleDateFormat("dd-MM-yyyy").parse(req.getParameter("fechaNacimiento")));
            estudianteService.guardar(estudiante);
            req.setAttribute("mensajeExitoso", "Datos del estudiante guardados correctamente.");
            req.getRequestDispatcher("/jsp/estudiante.jsp").forward(req, resp);
        } catch (ParseException e) {
            throw new ServletException("La fecha de nacimiento no tiene el formato correcto.", e);
        } catch (Exception e) {
            e.printStackTrace();
            req.setAttribute("mensajeError", "No se pudo guardar los datos del estudiante.");
            req.getRequestDispatcher("/jsp/estudiante.jsp").forward(req, resp);
        }
    }

    @Override
    public void init() throws ServletException {
        super.init();
        estudianteService = new EstudianteService();
    }

}
