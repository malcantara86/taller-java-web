package mx.com.discere.workshop.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.discere.workshop.service.EstudianteService;
import mx.com.discere.workshop.values.Estudiante;

@WebServlet(name = "ListaEstudiantesServlet", urlPatterns = { "/ListaEstudiantesServlet", "/ListaEstudiantes.do" })
public class ListaEstudiantesServlet extends HttpServlet {

    private static final long serialVersionUID = -1212636176459313213L;
    private EstudianteService estudianteService;

    @Override
    public void init() throws ServletException {
        super.init();
        estudianteService = new EstudianteService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Estudiante> estudiantes = new ArrayList<>();
        try {
            estudiantes = estudianteService.obtenerEstudiantes();

        } catch (Exception e) {
            e.printStackTrace();
            req.setAttribute("mensajeError", "No se pudo obtener la lista de estudiantes.");
        }
        req.setAttribute("estudiantes", estudiantes);
        req.getRequestDispatcher("jsp/estudiantes.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // TODO Auto-generated method stub
        super.doPost(req, resp);
    }

}
