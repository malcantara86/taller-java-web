package mx.com.discere.workshop.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.com.discere.workshop.util.DataSourceUtil;
import mx.com.discere.workshop.values.Estudiante;

public class EstudianteDAO {
    public void guardar(Estudiante estudiante) throws SQLException, Exception {
        String sql = "insert into estudiante (id, nombres, apellido_paterno, apellido_materno, fecha_nacimiento, telefono_celular) "
                + "values (nextval('estudiante_id_sequence'), ?, ?, ?, ?, ?)";
        PreparedStatement insertStatement = DataSourceUtil.getConnection().prepareStatement(sql);
        insertStatement.setString(1, estudiante.getNombres());
        insertStatement.setString(2, estudiante.getApellidoPaterno());
        insertStatement.setString(3, estudiante.getApellidoMaterno());
        insertStatement.setDate(4, new Date(estudiante.getFechaNacimiento().getTime()));
        insertStatement.setString(5, estudiante.getTelefonoCelular());
        insertStatement.executeUpdate();
        insertStatement.close();
    }

    public List<Estudiante> obtenerEstudiantes() throws SQLException, Exception {
        final List<Estudiante> estudiantes = new ArrayList<>();
        String sql = "select id, nombres, apellido_paterno, apellido_materno, fecha_nacimiento, telefono_celular from estudiante";
        PreparedStatement selectStatemet = DataSourceUtil.getConnection().prepareStatement(sql);
        ResultSet results = selectStatemet.executeQuery();
        Estudiante estudiante = null;
        while (results.next()) {
            estudiante = new Estudiante();
            estudiante.setId(results.getLong("id"));
            estudiante.setNombres(results.getString("nombres"));
            estudiante.setApellidoPaterno(results.getString("apellido_paterno"));
            estudiante.setApellidoMaterno(results.getString("apellido_materno"));
            estudiante.setFechaNacimiento(results.getDate("fecha_nacimiento"));
            estudiante.setTelefonoCelular(results.getString("telefono_celular"));
            estudiantes.add(estudiante);
        }
        return estudiantes;
    }
}
