<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Estudiante</title>
</head>
<body>
    <h2>Alta de estudiantes</h2>
    <form action="../AltaEstudiante.do" method="post">
        <table>
           <tr>
                <td>Nombre(s):</td>
                <td><input type="text" name="nombres" id="nombres" /></td>
           </tr>
           <tr>
                <td>Apellido paterno:</td>
                <td><input type="text" name="apellidoPaterno" id="apellidoPaterno" /></td>
           </tr>
           <tr>
                <td>Apellido materno:</td>
                <td><input type="text" name="apellidoMaterno" id="apellidoMaterno" /></td>
           </tr>
           <tr>
                <td>Fecha de nacimiento:</td>
                <td><input type="text" name="fechaNacimiento" id="fechaNacimiento" /></td>
           </tr>
           <tr>
                <td>Telefono celular:</td>
                <td><input type="text" name="telefonoCelular" id="telefonoCelular" /></td>
           </tr>
           <tr>
                <td colspan="2"><input type="submit" value="Guardar"/></td>
           </tr>
        </table>
    </form>
    <a href="index.html">Ir a la pagina principal.</a>
</body>
</html>