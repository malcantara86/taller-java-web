<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Estudiantes</title>
</head>
<body>
    <h2>Estudiantes</h2>
    <table border="1">
        <tr>
            <th>Nombre(s)</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Fecha nacimiento</th>
            <th>Telefono celular</th>
        </tr>
        <c:forEach items="${estudiantes}" var="estudiante">
            <tr>
                <td>${estudiante.nombres}</td>
                <td>${estudiante.apellidoPaterno}</td>
                <td>${estudiante.apellidoMaterno}</td>
                <td><fmt:formatDate
                        value="${estudiante.fechaNacimiento}"
                        pattern="dd-MM-yyyy" /></td>
                <td>${estudiante.telefonoCelular}</td>
            </tr>
        </c:forEach>
    </table>
    <a href="index.html">Ir a la pagina principal.</a>
</body>
</html>